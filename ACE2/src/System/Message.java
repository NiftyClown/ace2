package System;

import java.io.Serializable;

public interface Message extends Serializable {
	
	// Set the counts for characters and digits
	public void setCounts();
	
	// Return the number of characters
	public int getCharacterCount();
	
	// Return the number of digits
	public int getDigitCount();

}
