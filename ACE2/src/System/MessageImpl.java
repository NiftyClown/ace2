package System;


/**
 * Implementation of the Message interface
 * 
 * @author Barry McAuley
 * @reg 201106816
 *
 * @since Nov 12, 2013
 */

public class MessageImpl implements Message {
	
	private static final long serialVersionUID = -5172483113798115098L;
	private int digitCount, characterCount;
	private String message;
	
	public MessageImpl(String mess) {
		this.message = mess;
		digitCount = characterCount = 0;
	}

	@Override
	public void setCounts() {
		for(int i = 0; i < message.length(); i++) {
			if (message.charAt(i) >= 48 && message.charAt(i) <= 57) 
				++digitCount;
			else if (message.charAt(i) > 32) // No escape or white space characters
				++characterCount;
		}
	}

	@Override
	public int getCharacterCount() {
		return this.characterCount;
	}

	@Override
	public int getDigitCount() {
		return this.digitCount;
	}

}
