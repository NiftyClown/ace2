package Tests;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import System.MessageImpl;


public class MessageTest {
	
	private MessageImpl m1, m2, m3, m4;

	@Before
	public void setUp() throws Exception {
		m1 = new MessageImpl("Hello 123");
		m2 = new MessageImpl("Hello");
		m3 = new MessageImpl("123");
		m4 = new MessageImpl("");
	}

	@Test
	public void testGetCharacterCount() {
		assertEquals("The character count for m1 is wrong", 5, m1.getCharacterCount());
		assertEquals("The character count for m2 is wrong", 5, m2.getCharacterCount());
		assertEquals("The character count for m3 is wrong", 0, m3.getCharacterCount());
		assertEquals("The character count for m4 is wrong", 0, m4.getCharacterCount());
	}
	
	@Test
	public void testGetDigitCount() {
		assertEquals("The digit count for m1 is wrong", 3, m1.getDigitCount());
		assertEquals("The digit count for m2 is wrong", 0, m2.getDigitCount());
		assertEquals("The digit count for m3 is wrong", 3, m3.getDigitCount());
		assertEquals("The digit count for m4 is wrong", 0, m4.getDigitCount());
	}

}
