package Client;

import java.util.Scanner;

/**
 * A basic textual interface for the program so that
 * users can input a string of their desire and
 * send it to the server for processing.
 * 
 * @author Barry McAuley
 * @reg 201106816
 *
 * @since Nov 14, 2013
 */

public class UserInterface {
	
	private String message;
	private Scanner scan;
	
	/** Main method to kick it all off */
	public static void main(String args[]) {
		UserInterface p = new UserInterface();
	}
	
	public UserInterface() {
		scan = new Scanner(System.in);
		prompt();
	}
	
	/** Prompt the user for their input */
	private void prompt() {
		System.out.println("\t\tWelcome to ACE2!\n\n\tAuthor: Barry McAuley \t Reg: 201106816 \n\tVersion: 1.0 \t\t Since: 14/11/2013\n");
		while(true) {
			System.out.println("\nPlease select a choice: \n 1: Enter message \n 2: Exit system");
			System.out.print(">> ");
			String input = scan.nextLine();
			switch(input) {
				case("1"):
					System.out.println("\nPlease enter the message you wish to find the character and digit count of:");
					System.out.print(">> ");
					message = scan.nextLine();
					Client c = new Client(message); //Create the client and request
					break;
					
				case("2"):
					System.out.println("\nThank you for using ACE2. \nGood bye...");
					System.exit(0);
					break;
					
				default:
					System.out.println("Error with input. Please enter a valid choice.\n");
					break;
			}
		}
	}

}
