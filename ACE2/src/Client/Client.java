package Client;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import System.Message;

/**
 * A basic representaton of a client
 * for the exercise.
 * 
 * @author Barry McAuley
 * @reg 201106816
 *
 * @since Nov 13, 2013
 */

public class Client {
	
	private static final String hostName = "127.0.0.1";
	private static final int port = 6100;
	private Socket sock;
	
	public Client(String mess) {
		// Let's set everything up
		try {
			if (this.connect()) { // Connection established?
				this.sendMessage(mess);
				this.readMessage();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private boolean connect() throws UnknownHostException, IOException {
		System.out.println("\nConnecting to: " + hostName + " on port: " + port );
		try {
			sock = new Socket(hostName, port);
			System.out.println("Connection established");
			return true;
		} catch (Exception e) {
			System.out.println("\nConnection failed. Server unavailable.");
		}
		return false;
	}
	
	private void sendMessage(String mess) throws IOException {
		// Set up the stream
		DataOutputStream output = new DataOutputStream(sock.getOutputStream());
		// Write our message
		output.writeChars(mess + "\n");
	}
	
	private void readMessage() throws IOException, ClassNotFoundException {
		// Set up the stream and read the object
		ObjectInputStream input = new ObjectInputStream(sock.getInputStream());
		Message m = (Message) input.readObject();
		System.out.println("Message received");
		
		// Set counts and print the values
		m.setCounts();
		System.out.println("\nCharacter count of your message: " + m.getCharacterCount());
		System.out.println("Digit count of your message: " + m.getDigitCount());
		
		// Close up shop
		sock.close();
	}
	
}