package Server;

/**
 * A thread implementation.
 * Handles the request of the client.
 * 
 * @author Barry
 * @since 13/11/2013
 * 
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.net.Socket;

import System.Message;
import System.MessageImpl;

public class Worker implements Runnable {

    private Socket clientSocket;

    public Worker(Socket clientSocket) {
        this.clientSocket = clientSocket; //Initialise the socket
    }

    public void run() {
        try {
        	long startTime = System.currentTimeMillis();
        	System.out.println("Current running thread ID: " + Thread.currentThread().getId());
        	// Set up streams
            BufferedReader in   = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            ObjectOutputStream output = new ObjectOutputStream(clientSocket.getOutputStream());
            
            // Time to read the input and create the message
            String line = in.readLine();
            Message m = new MessageImpl(line);
            assert m != null: "Message is null";
            
            // Let's write to the socket
            output.writeObject(m);

            // Close up shop
            output.close();
            in.close();
            System.out.println("Request serviced in: " + (System.currentTimeMillis() - startTime) + " ms\n");
        } catch (IOException e) {
            //report exception somewhere.
            e.printStackTrace();
        }
    }
}
