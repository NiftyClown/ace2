package Server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Implementation of the ServerADT
 * 
 * @author Barry McAuley
 * @reg 201106816
 *
 * @since Nov 14, 2013
 */

public class Server  {
	
	private static final int port = 6100;
	private ExecutorService threadPool;

	/** Main method */
	public static void main(String[] args) {
		try {
			Server s = new Server();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public Server () throws IOException {
		System.out.println("\t\tWelcome to the ACE2 server!\n\n\tAuthor: Barry McAuley \t Reg: 201106816 \n\tVersion: 1.0 \t\t Since: 14/11/2013\n");
		
		ServerSocket sock = new ServerSocket(port);
		threadPool = Executors.newCachedThreadPool();
		
		// Listening...
		while (true) {
			Socket client = null;

			try {	
				client = sock.accept(); // Found a connection
				assert client != null: "Client is null";
				System.out.println("Client found: " + client);
				System.out.println("Assigning a thread...");
			}
			catch (IOException ioe) {
					System.err.println(ioe);
					break;
			}
			
			// Assign a thread and execute
			this.threadPool.execute(new Worker(client));
		}
		
		sock.close();
	}
	

}
